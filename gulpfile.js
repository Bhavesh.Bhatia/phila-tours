require("./gulp/tasks/css");
require("./gulp/tasks/watch");
require("./gulp/tasks/sprites");
require("./gulp/tasks/scripts");
require("./gulp/tasks/build");


//This is the syntax for gulp version before 4
//gulp.task("watch", function(){
//    watch("./app/index.html", function(){
//        gulp.start('html');
//    });
//    
//    watch("./app/assets/styles/**/*.css", function(){
//        gulp.start('css');
//    });
//});

